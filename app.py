from flask import *
import os
import sys
import datetime
from pathlib import Path
from getmac import get_mac_address
from functools import wraps
from tinydb import TinyDB, Query
from werkzeug.utils import secure_filename

class DateTimeSerializer():
    OBJ_CLASS = datetime.datetime  # The class this serializer handles

    def encode(self, obj):
        return obj.strftime('%Y-%m-%d')

    def decode(self, s):
        return datetime.datetime.strptime(s, '%Y-%m-%d')

UPLOAD_FOLDER = '/static/images'
ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg']

app = Flask(__name__)
app.secret_key = '8uc1-secret-key'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
db = TinyDB('db.json')
users = db.table('users')
payments = db.table('payments')
bills = db.table('bills')

ADMIN = 'don'

# PRE-PROCESSORS

@app.context_processor
def override_url_for():
    return dict(url_for=dated_url_for)

def dated_url_for(endpoint, **values):
    if endpoint == 'static':
        filename = values.get('filename', None)
        if filename:
            file_path = os.path.join(app.root_path,
                                    endpoint, filename)
            values['q'] = int(os.stat(file_path).st_mtime)
    return url_for(endpoint, **values)

# DECORATORS

def allowed_devices_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        mac_address = get_mac_address(
            ip="{}".format(
                request.environ.get(
                    'HTTP_X_REAL_IP',
                    request.remote_addr
                )
            )
        )
        is_admin = True
        Users = Query()
        MAC_ADDRESS = Query()
        user = users.search(
            Users.mac_addresses.any(MAC_ADDRESS.mac_address == mac_address)
        )
        print(user)
        if len(user) > 0:
            if user[0]['name'] != ADMIN:
                is_admin = False
        else:
            return "Invalid user."
        return f(user[0], is_admin, *args, **kwargs)
    return wrap

# PAGES

@app.route('/')
@allowed_devices_required
def index(user, is_admin):
    session['user'] = user['name']
    return render_template(
        'index.html',
        user=user,
        is_admin=is_admin
    )

# REST

@app.route('/rest/fetch/data')
def rest_fetch_data():
    try:
        today = datetime.datetime.today()
        _ = {
            table:db.table(
                table
            ).all()[::-1] for table in db.tables() if(
                table != 'users' and table != 'payments'
            )
        }
        Payments = Query()
        __ = [
            'meralco',
            'pldt',
            'water',
            'rent'
        ]
        _['payments'] = {
            ___:payments.search(
                (Payments.payer == session['user']) & (Payments.biller == ___) 
            )[::-1] for ___ in __
        }
        _['all_payments'] = [
            payments.search(
                Payments.payer == session['user']
            )[::-1]
        ]
        _['n_users'] = len(db.table('users'))
        return Response(
            json.dumps({
                'data': _
            }),
            200,
            mimetype='application/json'
        )
    except Exception as e:
        return Response(
            json.dumps({
                'error': e
            }),
            500,
            mimetype='application/json'
        )

@app.route('/rest/payments/new', methods=['POST', 'GET'])
def rest_payments_new():
    try:
        if request.method == 'POST':
            Bill = Query()
            payer_string = request.form['payer']
            biller_string = request.form['biller']
            _ = payments.insert({
                'amount': float(request.form['amount']),
                'payer': payer_string,
                'biller': biller_string,
                'timestamp': '{}'.format(
                    datetime.datetime.today()
                )
            })
            if 'payment-image' in request.files:
                f = request.files['payment-image']
                if f.filename != '':
                    if f and check_if_allowed_file(f.filename):
                        filename = secure_filename(
                            f'{payer_string}_{biller_string}_{_}.png'
                        )
                        filepath = f"""{Path(__file__).parent.absolute()}{
                            os.path.join(
                                app.config['UPLOAD_FOLDER'],
                                filename
                        )}"""
                        f.save(filepath)
                        payments.update({
                            'filename': filename
                        }, doc_ids=[
                            len(payments)
                        ])
            return Response(
                json.dumps({
                    'data': 'payment'
                }),
                200,
                mimetype='application/json'
            )
    except Exception as e:
        return Response(
            json.dumps({
                'error': e
            }),
            200,
            mimetype='application/json'
        )
    return redirect(url_for('index'))

@app.route('/rest/bills/new', methods=['POST', 'GET'])
def rest_bills_new():
    try:
        if request.method == 'POST':
            biller_string = request.form['biller']
            due_date = list(map(int, '{}'.format(request.form['due-date']).split('/')))
            biller = db.table(biller_string)
            _ = biller.insert({
                'amount': float(request.form['amount']),
                'biller': biller_string,
                'due_date': '{}'.format(datetime.date(
                    due_date[2],
                    due_date[1],
                    due_date[0]
                )),
                'timestamp': '{}'.format(
                    datetime.datetime.today()
                )
            })
            if 'bill-image' in request.files:
                f = request.files['bill-image']
                if f.filename != '':
                    if f and check_if_allowed_file(f.filename):
                        filename = secure_filename(
                            f'{biller_string}_{_}.png'
                        )
                        filepath = f"""{Path(__file__).parent.absolute()}{
                            os.path.join(
                                app.config['UPLOAD_FOLDER'],
                                filename
                        )}"""
                        f.save(filepath)
                        biller.update({
                            'filename': filename
                        }, doc_ids=[
                            len(biller)
                        ])
            return Response(
                json.dumps({
                    'data': 'bill'
                }),
                200,
                mimetype='application/json'
            )
    except Exception as e:
        return Response(
            json.dumps({
                'error': e
            }),
            200,
            mimetype='application/json'
        )
    return redirect(url_for('index'))

# UTILITIES

def get_file_extension(filename):
    fliename = filename.split('.')
    return fliename[len(fliename)-1].lower()

def check_if_allowed_file(filename):
    return '.' in filename and get_file_extension(filename) in ALLOWED_EXTENSIONS

# DRIVER

if __name__ == "__main__":
    app.run(
        '0.0.0.0',
        port=5000,
        debug=True
    )
