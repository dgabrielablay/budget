__author__ = 'Ablay'

class get_node: 
    def __init__(self, data):  
        self.data = data  
        self.left = self.right = None

def s_(root, s, l, max_len, max_sum): 
    if (not root): 
        if (max_len[0] < l):  
            max_len[0] = l
            max_sum[0] = s
        elif (max_len[0]== l and 
              max_sum[0] < s):  
            max_sum[0] = s
        return

    s_(root.left, s + root.data, l + 1, max_len, max_sum)  
    
    s_(root.right, s + root.data, l + 1, max_len, max_sum) 

def main(root): 
    if (not root):  
        return 0
    max_sum = [float('-inf')] 
    max_len = [0]
    s_(root, 0, 0, max_len, max_sum)
    return max_sum[0]

if __name__ == '__main__':
    root = get_node(4)
    root.left = get_node(2)
    root.right = get_node(5)
    root.left.left = get_node(7)
    root.left.right = get_node(1)
    root.right.left = get_node(2)
    root.right.right = get_node(3)
    root.left.right.left = get_node(6)
  
    print(main(root))