window.onload = function(){
    let paymentImageForm = document.getElementById('payment-image-form');
    let paymentImage = document.getElementById('payment-image');
    let newPaymentForm = document.getElementById('new-payment-form');
    let billImageForm = document.getElementById('bill-image-form');
    let billImage = document.getElementById('bill-image');
    let newBillForm = document.getElementById('new-bill-form');
    let main = document.getElementById('main');
    let bills = document.getElementsByClassName('bill');
    let addPayment = document.getElementById('addPayment');
    let addPaymentForm = document.getElementById('add-payment-form');
    let addPaymentClose = document.getElementById('add-payment-close');
    let cards = document.getElementsByClassName('animate__animated');
    let addBill = document.getElementById('addBill');
    let addBillForm = document.getElementById('add-bill-form');
    let addBillClose = document.getElementById('add-bill-close');
    let currentMeralco = document.getElementById('current-meralco');
    let currentWater = document.getElementById('current-water');
    let currentPldt = document.getElementById('current-pldt');
    let currentRent = document.getElementById('current-rent');
    let allRent = document.getElementById('allRent');
    let allMeralco = document.getElementById('allMeralco');
    let allWater = document.getElementById('allWater');
    let allPldt = document.getElementById('allPldt');
    let totalBillAmountObject = document.getElementById('totalBillAmount');
    let payments = document.getElementById('payments');

    let picker = new Pikaday({
        field: document.getElementById('datepicker'),
        format: 'D/M/YYYY',
        toString(date, format) {
            const day = date.getDate();
            const month = date.getMonth() + 1;
            const year = date.getFullYear();
            return `${day}/${month}/${year}`;
        },
        parse(dateString, format) {
            const parts = dateString.split('/');
            const day = parseInt(parts[0], 10);
            const month = parseInt(parts[1], 10) - 1;
            const year = parseInt(parts[2], 10);
            return new Date(year, month, day);
        }
    });
    fetchData();
    setInterval(() => {
        fetchData();
    }, 5000);
    newPaymentForm.addEventListener('submit', function(event){
        event.preventDefault();
        fetch(`${window.location.origin}/rest/payments/new`, {
            method: 'POST',
            body: new FormData(event.target)
        }).then(function(response){
            if(response.ok){
                return response.json();
            }
            return Promise.reject(response);
        }).then(function(data){
            timeout = 1250;
            setTimeout(() => {
                addPaymentForm.classList.remove('animate__animated');
                addPaymentForm.classList.add('display-none');
            }, timeout);
            addPaymentForm.classList.remove('animate__fadeInUp');
            addPaymentForm.classList.add('animate__fadeOutDown');
            timeoutGap = 100;
            homeFadeIn(timeout, timeoutGap, cards)
        }).catch(function(error){
            console.warn(error);
        });
    });
    paymentImageForm.addEventListener('change', function(){
        readURL(paymentImage, this);
    });
    newBillForm.addEventListener('submit', function(event){
        event.preventDefault();
        fetch(`${window.location.origin}/rest/bills/new`, {
            method: 'POST',
            body: new FormData(event.target)
        }).then(function(response){
            if(response.ok){
                return response.json();
            }
            return Promise.reject(response);
        }).then(function(data){
            timeout = 1250;
            setTimeout(() => {
                addBillForm.classList.remove('animate__animated');
                addBillForm.classList.add('display-none');
            }, timeout);
            addBillForm.classList.remove('animate__fadeInUp');
            addBillForm.classList.add('animate__fadeOutDown');
            timeoutGap = 100;
            homeFadeIn(timeout, timeoutGap, cards)
        }).catch(function(error){
            console.warn(error);
        });
    });
    billImageForm.addEventListener('change', function(){
        readURL(billImage, this);
    });
    for(const bill of bills){
        bill.addEventListener('click', function(event){
            event.preventDefault();
            let initialMarginRight = main.style.marginRight;
            let modal = document.getElementById(bill.dataset.modal);
            let width = "100vw";
            if(window.screen.width > 568){
                width = '350px';
            }
            modal.style.width = width;
            main.style.marginRight = initialMarginRight;
            main.style.filter = 'blur(1rem)';
            let closeButton = modal.childNodes[modal.childNodes.length - 2];
            closeButton.addEventListener('click', function(){
                modal.style.width = '0';
                main.style.filter = '';
                main.style.pointerEvents = '';
            });
        });
    }
    if(typeof(addPayment) != 'undefined' && addPayment != null){
        addPayment.addEventListener('click', function(event){
            event.preventDefault();
            timeout = 10;
            timeoutGap = 100;
            homeFadeOut(timeout, timeoutGap, cards);
            setTimeout(() => {
                addPaymentForm.classList.remove('display-none');
                addPaymentForm.classList.add('animate__animated');
                addPaymentForm.classList.add('animate__fadeInUp');
            }, timeout+1250);
            addPaymentClose.addEventListener('click', function(event){
                event.preventDefault();
                timeout = 1250;
                setTimeout(() => {
                    addPaymentForm.classList.remove('animate__animated');
                    addPaymentForm.classList.add('display-none');
                }, timeout);
                addPaymentForm.classList.remove('animate__fadeInUp');
                addPaymentForm.classList.add('animate__fadeOutDown');
                timeoutGap = 100;
                homeFadeIn(timeout, timeoutGap, cards)
            });
        });
    }
    if(typeof(addBill) != 'undefined' && addBill != null){
        addBill.addEventListener('click', function(event){
            event.preventDefault();
            timeout = 10;
            timeoutGap = 100;
            homeFadeOut(timeout, timeoutGap, cards);
            setTimeout(() => {
                addBillForm.classList.remove('display-none');
                addBillForm.classList.add('animate__animated');
                addBillForm.classList.add('animate__fadeInUp');
            }, timeout+1250);
            addBillClose.addEventListener('click', function(event){
                event.preventDefault();
                timeout = 1250;
                setTimeout(() => {
                    addBillForm.classList.remove('animate__animated');
                    addBillForm.classList.add('display-none');
                }, timeout);
                addBillForm.classList.remove('animate__fadeInUp');
                addBillForm.classList.add('animate__fadeOutDown');
                timeoutGap = 100;
                homeFadeIn(timeout, timeoutGap, cards)
            });
        });
    }

    var numDaysBetween = function(d1, d2) {
        var diff = Math.abs(d1.getTime() - d2.getTime());
        return diff / (1000 * 60 * 60 * 24);
    };

    function fetchData(){
        fetch(`${window.location.origin}/rest/fetch/data`)
        .then(function(response){
            if(response.ok){
                return response.json();
            }
            return Promise.reject(response);
        }).then(function(data){
            return data.data;
        }).then(function(data){
            let totalBillAmount = 0;
            if(data.rent){
                let latestRent = data.rent[0]
                let currentRentAmount = latestRent.amount / data.n_users;
                currentRent.childNodes[
                    5
                ].innerHTML = "due on: " + latestRent.due_date;
                if(data.payments.rent.length > 0){
                    let totalPaymentForThisMonth = 0;
                    for(const payments_rent of data.payments.rent){
                        if(numDaysBetween(
                            new Date(latestRent.due_date),
                            new Date(payments_rent.timestamp)
                        ) <= 25){
                            totalPaymentForThisMonth += payments_rent.amount;
                        }
                    }
                    currentRentAmount -= totalPaymentForThisMonth;
                }
                totalBillAmount += currentRentAmount;
                currentRent.childNodes[1].innerHTML = Number(
                    currentRentAmount
                ).toLocaleString(
                    undefined,
                    {
                        maximumFractionDigits: 2,
                        minimumFractionDigits: 2
                    }
                );
                allRentHtml = ''
                for(const rent of data.rent){
                    allRentHtml += addBillData(rent);
                }
                if(allRent.innerHTML != allRentHtml){
                    allRent.innerHTML = allRentHtml;
                }
            }
            if(data.water){
                let latestWater = data.water[0]
                let currentWaterAmount = latestWater.amount / data.n_users;
                currentWater.childNodes[
                    5
                ].innerHTML = "due on: " + latestWater.due_date;
                if(data.payments.water.length > 0){
                    let totalPaymentForThisMonth = 0;
                    for(const payments_water of data.payments.water){
                        if(numDaysBetween(
                            new Date(latestWater.due_date),
                            new Date(payments_water.timestamp)
                        ) <= 25){
                            totalPaymentForThisMonth += payments_water.amount;
                        }
                    }
                    currentWaterAmount -= totalPaymentForThisMonth;
                }
                totalBillAmount += currentWaterAmount;
                currentWater.childNodes[1].innerHTML = Number(
                    currentWaterAmount
                ).toLocaleString(
                    undefined,
                    {
                        maximumFractionDigits: 2,
                        minimumFractionDigits: 2
                    }
                );
                allWaterHtml = ''
                for(const water of data.water){
                    allWaterHtml += addBillData(water);
                }
                if(allWater.innerHTML != allWaterHtml){
                    allWater.innerHTML = allWaterHtml;
                }
            }
            if(data.pldt){
                let latestPldt = data.pldt[0]
                let currentPldtAmount = latestPldt.amount / data.n_users;
                currentPldt.childNodes[
                    5
                ].innerHTML = "due on: " + latestPldt.due_date;
                if(data.payments.pldt.length > 0){
                    let totalPaymentForThisMonth = 0;
                    for(const payments_pldt of data.payments.pldt){
                        if(numDaysBetween(
                            new Date(latestPldt.due_date),
                            new Date(payments_pldt.timestamp)
                        ) <= 25){
                            totalPaymentForThisMonth += payments_pldt.amount;
                        }
                    }
                    currentPldtAmount -= totalPaymentForThisMonth;
                }
                totalBillAmount += currentPldtAmount;
                currentPldt.childNodes[1].innerHTML = Number(
                    currentPldtAmount
                ).toLocaleString(
                    undefined,
                    {
                        maximumFractionDigits: 2,
                        minimumFractionDigits: 2
                    }
                );
                allPldtHtml = ''
                for(const pldt of data.pldt){
                    allPldtHtml += addBillData(pldt);
                }
                if(allPldt.innerHTML != allPldtHtml){
                    allPldt.innerHTML = allPldtHtml;
                }
            }
            if(data.meralco){
                let latestMeralco = data.meralco[0]
                let currentMeralcoAmount = latestMeralco.amount / data.n_users;
                currentMeralco.childNodes[
                    5
                ].innerHTML = "due on: " + latestMeralco.due_date;
                if(data.payments.meralco.length > 0){
                    let totalPaymentForThisMonth = 0;
                    for(const payments_meralco of data.payments.meralco){
                        if(numDaysBetween(
                            new Date(latestMeralco.due_date),
                            new Date(payments_meralco.timestamp)
                        ) <= 25){
                            totalPaymentForThisMonth += payments_meralco.amount;
                        }
                    }
                    currentMeralcoAmount -= totalPaymentForThisMonth;
                }
                totalBillAmount += currentMeralcoAmount;
                currentMeralco.childNodes[1].innerHTML = Number(
                    currentMeralcoAmount
                ).toLocaleString(
                    undefined,
                    {
                        maximumFractionDigits: 2,
                        minimumFractionDigits: 2
                    }
                );
                allMeralcoHtml = ''
                for(const meralco of data.meralco){
                    allMeralcoHtml += addBillData(meralco);
                }
                if(allMeralco.innerHTML != allMeralcoHtml){
                    allMeralco.innerHTML = allMeralcoHtml;
                }
            }
            totalBillAmountObject.innerHTML = Number(
                totalBillAmount
            ).toLocaleString(
                undefined,
                {
                    maximumFractionDigits: 2,
                    minimumFractionDigits: 2
                }
            );
            if(data.all_payments){
                if(data.all_payments[0].length > 0){
                    let paymentHtml = '';
                    for(const payment of data.all_payments[0]){
                        paymentHtml += addPaymentData(payment);
                    }
                    if(payments.innerHTML != paymentHtml){
                        payments.innerHTML = paymentHtml;
                    }
                }else{
                    payments.innerHTML = `
                        <div class="row my-2">
                            <div class="col">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">No payments yet...</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    `
                }
            }
        }).catch(function(error){
            console.warn(error);
        });
    }
};

function addPaymentData(payment){
    return `
        <div class="row my-2">
            <div class="col">
                <div class="card">
                    <img src="${
                        window.location.origin
                    }/static/images/${
                        payment.filename
                    }" alt="..." class="card-img-top">
                    <div class="card-body">
                        <div class="card-title">
                            <h5>₱ ${ Number(
                                payment.amount
                            ).toLocaleString(
                                undefined,
                                {
                                    maximumFractionDigits: 2,
                                    minimumFractionDigits: 2
                                }
                            ) }</h5>
                            <div class="card-description">
                                <p>Paid on ${payment.timestamp}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `;
}

function addBillData(bill){
    return `
        <div class="row mb-3">
            <div class="col">
                <div class="card">
                    <img src="${
                        window.location.origin
                    }/static/images/${
                        bill.filename
                    }" alt="..." class="card-img-top">
                    <div class="card-body">
                        <div class="card-title">
                            <h5>₱ ${ Number(
                                bill.amount
                            ).toLocaleString(
                                undefined,
                                {
                                    maximumFractionDigits: 2,
                                    minimumFractionDigits: 2
                                }
                            ) }</h5>
                            <div class="card-description">
                                <p>Due on ${bill.due_date}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `;
}

function removePx(val){
    return val.slice(0, -2);
}

function readURL(paymentImage, input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            paymentImage.src = e.target.result;
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}

function homeFadeOut(timeout, timeoutGap, cards){
    for(const card of cards){
        setTimeout(() => {
            card.classList.add('animate__fadeOutDown');
            setTimeout(() => {
                card.style.display = 'none';
            }, timeout + 1000);
        }, timeout);
        timeout += timeoutGap;
    }
}

function homeFadeIn(timeout, timeoutGap, cards){
    for(const card of cards){
        setTimeout(() => {
            card.style.display = 'block';
            card.classList.remove('animate__fadeOutDown');
            card.classList.add('animate__fadeInUp');
            setTimeout(() => {
                
            }, timeout + 1000);
        }, timeout);
        timeout += timeoutGap;
    }
}